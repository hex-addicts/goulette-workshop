package app

import (
	"fmt"

	"github.com/jroimartin/gocui"
)

func (app *App) quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func (app *App) selectView(view string) func(g *gocui.Gui, v *gocui.View) error {
	return func(g *gocui.Gui, v *gocui.View) error {
		_, err := g.SetCurrentView(view)

		return err
	}
}

func (app *App) sendMessage(g *gocui.Gui, v *gocui.View) error {
	v, err := g.View("input")
	if err != nil {
		return err
	}
	msg := v.Buffer()
	app.cli.Speak(msg)
	v.SetCursor(0, 0)
	v.Clear()

	return nil
}

func (app *App) nextChat(g *gocui.Gui, v *gocui.View) error {
	return app.cli.Next()
}

func (app *App) keybindings(g *gocui.Gui) error {

	bindings := []struct {
		view    string
		key     interface{}
		mod     gocui.Modifier
		handler func(*gocui.Gui, *gocui.View) error
	}{
		{"", gocui.KeyEnter, gocui.ModAlt, app.sendMessage},
		{"chatroll", gocui.MouseLeft, gocui.ModNone, app.selectView("chatroll")},
		{"", gocui.KeyCtrlQ, gocui.ModNone, app.quit},
		{"", gocui.KeyCtrlN, gocui.ModNone, app.nextChat},
	}

	for _, b := range bindings {
		if err := g.SetKeybinding(b.view, b.key, b.mod, b.handler); err != nil {
			return err
		}
	}

	return nil
}

func (app *App) layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	if v, err := g.SetView("header", 1, 1, maxX/2-1, 3); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "Goulette v0.1"
		fmt.Fprintf(v, "Next Chat: Ctrl+N | Quit: Ctrl+Q")
	}

	if v, err := g.SetView("status", maxX/2, 1, maxX-1, 3); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "Status"
		fmt.Fprintf(v, "Host: %s:%d  |  Nick: %s", app.host, app.port, app.nick)
	}

	if v, err := g.SetView("chatroll", 1, 4, maxX/2-1, maxY-7); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "Incoming messages"
		v.Autoscroll = true
	}

	if v, err := g.SetView("debug", maxX/2, 4, maxX-1, maxY-7); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Title = "Debug info"
		v.Autoscroll = true
	}

	if v, err := g.SetView("input", 1, maxY-6, maxX-1, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Editable = true
		v.Title = "Say something (send: Alt+Enter)"
	}

	_, err := g.SetCurrentView("input")
	return err
}

func (app *App) updateUI() {
	app.gui.Update(func(_ *gocui.Gui) error { return nil })
}
