package app

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"
	"gitlab.com/hex-addicts/goulette-workshop/client"
)

type App struct {
	cli  *client.Client
	gui  *gocui.Gui
	host string
	port uint
	nick string
}

func New(host string, port uint, nick string) *App {
	return &App{
		host: host,
		port: port,
		nick: nick,
	}
}

func (app *App) Run() error {
	if err := app.setupWebsockets(); err != nil {
		return fmt.Errorf("app setup websockets: %v", err)
	}

	if err := app.setupGUI(); err != nil {
		return fmt.Errorf("app setup GUI: %v", err)
	}

	if err := app.gui.MainLoop(); err != nil && err != gocui.ErrQuit {
		return fmt.Errorf("app main loop: %v", err)
	}

	return nil
}

func (app *App) Close() error {
	app.cli.Close()
	app.gui.Close()

	return nil
}

func (app *App) setupWebsockets() error {
	// TODO: #2 setup websocket connection and inject to the client
	// BONUS POINTS: wrap websocket connection in an implementation of Transport interface (transport/transport.go file)
	app.cli = client.New()

	return app.cli.Login(app.nick)
}

func (app *App) setupGUI() error {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		return fmt.Errorf("creating GUI: %v", err)
	}

	app.gui = g

	g.Cursor = true
	g.Mouse = true
	g.FgColor = gocui.ColorGreen

	g.SetManagerFunc(app.layout)

	if err := app.keybindings(g); err != nil {
		log.Panicln(err)
	}

	go func() {
		for msg := range app.cli.Listen() {
			app.displayIncomingMessage(msg, g)
		}
	}()

	go func() {
		for debugMsg := range app.cli.ListenDebug() {
			app.displayDebugMessage(debugMsg, g)
		}
	}()

	return nil
}

func (app *App) displayDebugMessage(debugMsg client.DebugMessage, g *gocui.Gui) {
	v, err := g.View("debug")
	if err != nil {
		panic(err)
	}

	var dataStr string

	switch v := debugMsg.Data.(type) {
	case []byte:
		dataStr = string(v)
	default:
		dataStr = fmt.Sprintf("%v", v)
	}

	fmt.Fprintf(v, "%s %s\n%s\n\n", debugMsg.Action, debugMsg.Key, dataStr)

	app.updateUI()
}

func (app *App) displayIncomingMessage(msg string, g *gocui.Gui) {
	v, err := g.View("chatroll")
	if err != nil {
		panic(err)
	}
	fmt.Fprintln(v, msg)
}
