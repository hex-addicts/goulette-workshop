package client

// TIP: you could add a send() method to group all command sending logic, and call debug() method,
// to log all outgoing commands in the debug section of the app.
// Do the same when receiving events from server.

type DebugMessage struct {
	Action string
	Key    string
	Data   interface{}
	Error  error
}

type Client struct {
	debugChs []chan DebugMessage
	// TODO: #3 add needed attributes (ie: Transport or websocket connection)
}

func New() *Client {
	// TODO: #4 init the client
	return &Client{}
}

func (cli *Client) Login(nick string) error {
	// TODO: #5 send login command to server
	return nil
}

func (cli *Client) Logout() error {
	// TODO: send logout command to server
	return nil
}

func (cli *Client) Speak(message string) error {
	// TODO: #6 send chat.send_message command to server
	return nil
}

func (cli *Client) Listen() <-chan string {
	// TODO: #7 allocate a string channel for returning incoming messages
	// - Span a new goroutine which reads chat messages from the socket
	// - Call debug() method with message data
	// - Return the allocated channel

	return nil
}

func (cli *Client) Next() error {
	// TODO: #8 send chat.cycle command to server
	return nil
}

func (cli *Client) Close() error {
	// TODO: #9 close websocket connection
	return nil
}

func (cli *Client) ListenDebug() <-chan DebugMessage {
	debugCh := make(chan DebugMessage, 10)
	cli.debugChs = append(cli.debugChs, debugCh)

	return debugCh
}

func (cli *Client) debug(msg DebugMessage) {
	for _, ch := range cli.debugChs {
		select {
		case ch <- msg:
		default: // needed to skip blocked channels
		}
	}
}
