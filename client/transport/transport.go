package transport

// Transport is used to send and receive data
type Transport interface {
	Send(key string, data interface{}) error
	Receive() (key string, data []byte, err error)
	DecodeInto(data []byte, dest interface{}) error
	Close() error
}
