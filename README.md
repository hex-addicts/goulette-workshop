# Goulette client

This repository contains the skeleton code for developing a very simple chat client in Go, as a learning exercise.

Your mission, should you choose to accept it, is to create a client to chat with other users in a Chatroulette fashion, ie: 
connect with a random user (or bot if no user is waiting), and cycle to another random connection when sending a "next" command.

The connection to the server is done via WebSockets.

The client sends commands to the server, and the server sends events to the client, both serialized using JSON.

Due to the naive implementation of the server, the commands and events are totally decoupled. That means, there's no way to relate an event with the command that originated it, as would happen in request-response based protocols.

Also, there's no implementation of message acknowledgement (ACK) from either part.

## Challenge guidelines

This challenge is a presented as a "fill-in the blanks" exercise.

There's already a text-based  UI app with a basic interface and key bindings for the basic functionality, but you need to implement the actual communication with the Goulette server.

The first thing you'll need is a working Go development environment:

### Setting up the development environment

- Download and install the latest stable version of Go. At the time of this writing that is `v1.11.2`

```bash
GOLANG_VERSION=1.11.2
url="https://golang.org/dl/go${GOLANG_VERSION}.linux-amd64.tar.gz"
wget -O go.tgz "$url"
sudo tar -C /usr/local -xzf go.tgz
rm go.tgz
```

- Create a folder for Go code:

```bash
mkdir -p ~/code/go/{bin,src}
```

- Add needed variables to your user's profile config (`~/.profile`)

```text
export GOPATH=~/code/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

- Log out of your current desktop session, and login again, so the changes take effect.

- Install [Visual Studio Code](https://code.visualstudio.com/) with the [Go extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.Go)

- Once the editor and extension are installed, start VS Code and open the Command Palette (Ctrl + Shift + P) and select Go: Install / Update tools.

- Check all tools and click OK. This will install all tools needed by the Go extension.

- Install Dep 
```bash
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

### Cloning the repository

For everything to work as expected, you need to clone the repository in a certain directory, under the `$GOPATH` folder.

So, first create the parent directory:

```bash
mkdir -p $GOPATH/src/gitlab.com/hex-addicts
```

And then, clone this repo inside that directory:

```bash
cd $GOPATH/src/gitlab.com/hex-addicts
git clone https://gitlab.com/hex-addicts/goulette-workshop.git
```

### Building and running the app

Now that we've cloned the repository, we'll cd to it and install vendored dependencies:

```bash
cd $GOPATH/src/gitlab.com/hex-addicts/goulette-workshop
dep ensure
```

To run the app, assuming you are in the root of this repository (where the `main.go` file resides), run:

```bash
go run . 
```

This will compile, link and run the application.

If you need to pass arguments and / or flags to the app (like the hostname and the user nick), add them **after** the "dot":

```bash
go run . arg1 arg2 -flag1 flag1val -flag2
```

### Implementing the missing code

Once you have the environment up and running, it's time to have fun! You'll see TODO items in the code, with brief explanations of what's missing there.

### Useful information

Here you can find some useful resources on how to write Go code, and perform some tasks which you'll need for finishing the exercise. 

You're not expected to read it all, since that would take days, but they can be handy as a reference.

#### Go coding: general guidelines and examples

- [How to write Go code](https://golang.org/doc/code.html)
- [Effective Go](https://golang.org/doc/effective_go.html)
- [A Tour of Go](https://tour.golang.org/welcome/1)
- [Go By Example](https://gobyexample.com/)

#### Command line argument parsing

You can have a look at the examples for command line [arguments](https://gobyexample.com/command-line-arguments) and [flags](https://gobyexample.com/command-line-flags) on Go By Example.

#### Json encoding / decoding

There are also some [nice examples](https://gobyexample.com/json) on Go By Example.

#### WebSockets

- Connecting with a WebSocket server:

```go
import (
    "golang.org/x/net/websocket"
)

serverHost := "ws://192.168.1.50:8080/wshandler"
origin := "http://localhost"
wsConn, err := websocket.Dial(serverHost, "", origin)

// IMPORTANT: do not forget to close connection when finished with it
defer wsConn.Close()
```

- Sending JSON data over a WebSocket connection:

```go
import (
    "golang.org/x/net/websocket"
)

// using a map here, but a struct would work just fine
cmd := map[string]interface{}{
    "command": "some.command",
    "data": map[string]interface{}{
        "foo": "bar",
        "maxItems": 200,
    },
}
err := websocket.JSON.Send(wsConn, cmd)
```

- Receiving JSON data from a WebSocket connection:

```go
import (
    "golang.org/x/net/websocket"
)

event := struct {
    Event string          `json:"event"`
    Data  json.RawMessage `json:"data"`
}{}

// IMPORTANT: pass a pointer to the struct so it can be modified
err := websocket.JSON.Receive(wsConn, &event)
```

## Connection life-cycle

Now let's have a quick overview to how the Goulette client / server interaction should work.

### Connection establishment

The first thing the client needs to do, is open a WebSocket conenction to the server.

For this, we suggest using the official WebSocket pacakge (golang.org/x/net/websocket)

You can find an example of a client connection [here](https://godoc.org/golang.org/x/net/websocket#example-Dial).

### Login

Once the websocket connection is established, a `connection.login` **command** should be sent to the server over the existing connection, with the nick for login in the message payload.

```json
{
    "command" : "connection.login",
    "data" : {
        "nick": "john.doe"
    }
}
```

### New chat

Once the server receives the login command, it will try to pair the user with another user who was waiting. Otherwise, a bot will be picked. Then, the server will issue a `"chat.new"` **event** to the client:

```json
{
    "event" : "chat.started",
    "data": {
        "participants": ["jack.the.ripper"]
    }
}
```

### Sending messages

When we're in a chat, we can send messages using the `"chat.send_message"` **command**:

```json
{
    "command" : "chat.send_message",
    "data" : {
        "message": "Ola ke ase!"
    }
}
```

If we send this command without being in a chat, the server will respond with an error.

### Receiving messages

When the other user of the chat sends a message to us, the server will send a `"chat.receive"` **event** with the message:

```json
{
    "event" : "chat.message_received",
    "data" : {
        "message": "Wasssssup!"
    }
}
```

### Next chat

When the user wants to end the current chat, and start a new one, a `"chat.cycle"` **command** will be issued. This command does not need any extra data:

```json
{
    "command" : "chat.cycle",
}
```

### Disconnect

Once we are done with chatting, we can send a `"connection.close"` **command** to the server, and it will close the connection from its end, or we can just close our WebSocket connection, which will have the same result.

### Error

If any command we send to the server produces an error, the server will send us an `"connection.error_occurred"` event with details of the error and the command that produced it.

```json
{
    "event" : "connection.error_occurred",
    "data" : {
        "error" : "nick is already being used: (john.doe)",
        "command" : "connection.login",
        "data" : {
            "nick" : "john.doe"
        }
    }
}
```

## Command reference

Here's a list of all the commands the server accepts:

### chat.cycle

Requests to end the current chat and start a new one.

```json
{
    "command" : "chat.cycle"
}
```

### chat.send_message

Sends a message to the current chat.

```json
{
    "command" : "chat.send_message",
    "data" : {
        "message": "Ola ke ase!"
    }
}
```

### connection.close

Requests the server to end the connection.

```json
{
    "command" : "connection.close"
}
```

### connection.login

Sets a nick for our user in the current connection. If the nick is already used in another connection in the server, an error will be returned.

```json
{
    "command" : "connection.login",
    "data" : {
        "nick": "john.doe"
    }
}
```

### connection.debug.enable

Enables debug mode for the current connection. Debug mode is just an "echo", where the server will send us an event (`"connection.command_received"`) with the received data for every command we send.

```json
{
    "command" : "connection.debug.enable"
}
```

### connection.debug.disable

Disables debug mode for the current connection.

```json
{
    "command" : "connection.debug.disable"
}
```

## Event reference

Here is a list of all the possible events the server can emit:

### connection.command_received

Emitted when the server receives a command, and we have `debug mode` active for our connection.

In this example, the command we sent to the server (`"connection.login"`) is embedded inside the event's data and sent back to us:

```json
{
    "event" : "connection.command_received",
    "data": {
        "command" : "connection.login",
        "data" : {
            "nick": "john.doe"
        }
    }
}
```

### connection.error_occurred

Similarly to the previous event, when an error occurs while processing a command we sent, the server will send us an event like this.

```json
{
    "event" : "connection.error_occurred",
    "data" : {
        "error" : "nick is already being used: (john.doe)",
        "command" : "connection.login",
        "data" : {
            "nick" : "john.doe"
        }
    }
}
```

### chat.message_received

When the other user in a chat sends us a message, the server will emit an event for us:

```json
{
    "event" : "chat.message_received",
    "data" : {
        "message": "Wasssssup!"
    }
}
```

### chat.started

When we enter a new chat, be it because we (or the other user) issued a `"chat.cycle"` command, or we were chatting with a bot and a human user connected, an event like this will be sent to us:

```json
{
    "event" : "chat.started",
    "data": {
        "participants": ["jack.the.ripper"]
    }
}
```

### chat.ended

You've probably guessed this one...

```json
{
    "event" : "chat.ended"
}
```