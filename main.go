package main

import (
	"log"

	"gitlab.com/hex-addicts/goulette-workshop/client/app"
)

func main() {
	//TODO: #1 parse command line flags for host, port and nick
	app := app.New("", 0, "")
	defer app.Close()
	log.Fatal(app.Run())
}
